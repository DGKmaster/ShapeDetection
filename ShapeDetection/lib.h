#pragma once
#include <opencv2\opencv.hpp>
#include <opencv2\highgui.hpp>
#include <opencv2\imgproc.hpp>
#include <iostream>
#include <cstdio>

class Lib
{
private:
	/// Object for storing camera properties
	cv::VideoCapture cap;

	/// Matrix for store current video frame
	cv::Mat input_image;
	/// Store grayscale image
	cv::Mat grayscale_image;
	/// Store blurred grayscale image
	cv::Mat blurred_grayscale_image;
	/// Array for storing all detected edges
	cv::Mat canny_image;
	/// Array for storing image with detected circles
	cv::Mat circles_image;
	/// Array for storing image in HSV format
	cv::Mat hsv_image;
	/// Array for storing red color from base image
	cv::Mat red_color_image;
	/// Array for set parameters
	cv::Mat parameters;
	/// Initialise frame for showing contours
	cv::Mat contours_image;

	/// Thresholds value for canny edge detector
	int thresh; // Current
	int max_thresh; // Maximum

					/// Thresholds value for HSV red color detector
	int H, S, V; // Current
	int Hmax, Smax, Vmax; // Maximum

						  /// Uniform distribution for set contour colors
	cv::RNG rng;

	// True - open video from cam, false - read image
	const bool CAM_OR_IMAGE = false;
public:
	/// Default constructor
	Lib();
	/// Default destructor
	~Lib();

	/// Show and update all parameters
	void updateParameters();

	bool getCamOrImage();

	/// Predict signs on image
	void svmPredict();

	/// Find and show all contours
	void getFigures();

	/// Get all image parts with red contours
	void getROI();

	/// Put label - black text on white rectangle - in contour center
	void setLabel(cv::Mat im, const std::string label, std::vector<cv::Point> contour);

	/// Get red colour from HSV
	void getRedColour();

	/// Detect all circles on current frame
	/// and overlay all
	void detectCircles();

	/// Show named image
	void showImage(std::string name);

	void prepareImage();

	/// Read image or get video frame
	void readImage(std::string image_name = "figures.jpg");
};