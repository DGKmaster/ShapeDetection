#include "lib.h"

Lib::Lib()
{
	/// Open camera
	cap = cv::VideoCapture(0);
	/// If camera is not opened
	/// close program
	if (!cap.isOpened())
	{
		std::cerr << "Can not open camera" << std::endl;
	}

	/// Thresholds value for HSV red color detector
	// Current values
	H = 146;
	S = 111;
	V = 218;
	// Maximum values
	Hmax = 221;
	Smax = 255;
	Vmax = 255;

	/// For set random color
	rng = 12345;

	/// Thresholds value for canny edge detector
	thresh = 30; // Current
	max_thresh = 255; // Maximum

					  /// Set array to show parameters
	parameters = cv::Mat::zeros(cv::Size(300, 80), CV_8UC1);
}

/// Read 
void Lib::readImage(std::string image_name)
{
	if (CAM_OR_IMAGE)
	{
		/// Read current video frame
		cap >> input_image;
	}
	else
	{
		/// Read image
		input_image = cv::imread(image_name, 1);
	}
}

/// Predict signs on image
void Lib::svmPredict()
{
	/*
	cv::HOGDescriptor hog = cv::HOGDescriptor();
	cv::resize(roi, roi, cv::Size(50, 50));
	std::vector<float> hog_features;
	hog.compute(roi, hog_features, cv::Size(10, 10), cv::Size(50, 50));

	for (int i = 0; i < hog_features.size(); i++)
	{
	training_data.at<float>(imagenum, i) = hog_features[i];
	}
	cv::Mat training_class = cv::Mat(NUM_TRAINING_SAMPLES, 1, CV_32FC1);
	training_class.at<float>(1, 0) = 1;
	training_class.at<float>(0, 0) = 2;

	cv::Ptr<cv::ml::SVM> svm = cv::ml::SVM::create();
	svm->setType(cv::ml::SVM::C_SVC);
	svm->setKernel(cv::ml::SVM::POLY);
	svm->setGamma(3);
	svm->setTermCriteria(cv::TermCriteria(cv::TermCriteria::MAX_ITER, 100, 1e-6));
	svm->train(training_data, cv::ml::ROW_SAMPLE, training_class);
	float result;
	result = svm->predict(test_sample);

	}
	*/
}

/// Get all image parts with red contours
void Lib::getROI()
{
	/// Array for storing all found contours
	std::vector<std::vector<cv::Point>> contours;
	/// Array for storing information about found contours
	std::vector<cv::Vec4i> hierarcy;
	cv::findContours(red_color_image, contours, hierarcy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0));

	/// Initialise resolution for showing contours
	cv::Mat signs_image = cv::Mat::zeros(red_color_image.size(), CV_8UC3);

	/// Array for storing all contour approximate curves
	std::vector<cv::Point> approx;


	std::vector<cv::Point> count;

	/// Draw all contours
	for (int i = 0; i < contours.size(); i++)
	{
		/// Set random contour color
		cv::Scalar color = cv::Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));

		/// Find all big polylines in contours
		cv::approxPolyDP(contours[i], approx, cv::arcLength(cv::Mat(contours[i]), true)*0.02, true);

		if ((std::fabs(cv::contourArea(contours[i])) > 600) && cv::isContourConvex(approx))
		{
			cv::Rect r = cv::boundingRect(contours[i]);
			cv::drawContours(signs_image, contours, i, cv::Scalar(255, 0, 0), CV_FILLED, 8, hierarcy);
			cv::rectangle(signs_image, r, cv::Scalar(0, 0, 255));

			cv::Mat roi = input_image(r);

			if (!roi.empty())
			{
				cv::imshow("ROI", roi);
			}
		}
	}
	cv::imshow("Signs", signs_image);
}

/// Find and show all contours
void Lib::getFigures()
{
	/// Array for storing all found contours
	std::vector<std::vector<cv::Point>> contours;
	/// Array for storing information about found contours
	std::vector<cv::Vec4i> hierarcy;

	/// Find all contours
	cv::findContours(canny_image, contours, hierarcy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0));
	/// Arrays for storing contour information
	std::vector<cv::Moments> mu(contours.size());
	std::vector<cv::Point2f> mc(contours.size());
	/// Find contour mass center
	for (int i = 0; i < contours.size(); i++)
	{
		mu[i] = cv::moments(contours[i], false);
		mc[i] = cv::Point2f(mu[i].m10 / mu[i].m00, mu[i].m01 / mu[i].m00);
	}

	/// Initialise resolution for showing contours
	contours_image = cv::Mat::zeros(canny_image.size(), CV_8UC3);

	/// Array for storing all contour approximate curves
	std::vector<cv::Point> approx;

	/// To count each figure type
	std::vector<cv::Point> count_3; // Number of triangles
	std::vector<cv::Point> count_4; // Number of rectangels
	std::vector<cv::Point> count_5; // Number of pentagons
	std::vector<cv::Point> count_6; // Number of hexagons
	std::vector<cv::Point> count_0; // Number of circles

									/// Flag for check that figure is not same
	bool new_figure;
	/// Minimal difference of figure mass centers
	/// to define that figure is new
	const int min_difference = 5;

	/// Draw all contours
	for (int i = 0; i < contours.size(); i++)
	{
		/// Set random contour color
		cv::Scalar color = cv::Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));

		/// Find all big polylines in contours
		cv::approxPolyDP(contours[i], approx, cv::arcLength(cv::Mat(contours[i]), true)*0.02, true);

		/// If contour is small or not convex go to next contour
		if (std::fabs(cv::contourArea(contours[i])) < 100 || !cv::isContourConvex(approx))
		{
			continue;
		}

		/// Set flag to true in each cycle
		new_figure = true;

		/// Put triangle label
		if (approx.size() == 3)
		{
			/// Go through all figures and verify its unique
			for (size_t j = 0; j < count_3.size(); j++)
			{
				/// If difference is small figure is not new
				if ((abs(mc[i].x - count_3[j].x) < min_difference) && (abs(mc[i].y - count_3[j].y) < min_difference))
				{
					/// Set flag to false to not draw and count it
					new_figure = false;
				}
			}
			/// If figure is new draw it and store its mass center
			if (new_figure)
			{
				/// Add new value
				count_3.push_back(mc[i]);

				/// Draw contour on frame
				cv::drawContours(contours_image, contours, i, color, 2, 8, hierarcy);
				/// Draw contour mass center
				cv::circle(contours_image, mc[i], 4, color, -1, 8, 0);
				/// Put label that this contour is triangle and count
				setLabel(contours_image, "TRI", contours[i]);
			}

		}
		/// Put rectangle label
		else if (approx.size() == 4)
		{
			/// Go through all figures and verify its unique
			for (size_t j = 0; j < count_4.size(); j++)
			{
				/// If difference is small figure is not new
				if ((abs(mc[i].x - count_4[j].x) < min_difference) && (abs(mc[i].y - count_4[j].y) < min_difference))
				{
					/// Set flag to false to not draw and count it
					new_figure = false;
				}
			}
			/// If figure is new draw it and store its mass center
			if (new_figure)
			{
				/// Add new value
				count_4.push_back(mc[i]);

				/// Draw contour on frame
				cv::drawContours(contours_image, contours, i, color, 2, 8, hierarcy);
				/// Draw contour mass center
				cv::circle(contours_image, mc[i], 4, color, -1, 8, 0);
				/// Put label that this contour is rectangle and count
				setLabel(contours_image, "QUAD", contours[i]);
			}
		}
		/// Put pentagon label
		else if (approx.size() == 5)
		{
			/// Go through all figures and verify its unique
			for (size_t j = 0; j < count_5.size(); j++)
			{
				/// If difference is small figure is not new
				if ((abs(mc[i].x - count_5[j].x) < min_difference) && (abs(mc[i].y - count_5[j].y) < min_difference))
				{
					/// Set flag to false to not draw and count it
					new_figure = false;
				}
			}
			/// If figure is new draw it and store its mass center
			if (new_figure)
			{
				/// Add new value
				count_5.push_back(mc[i]);

				/// Draw contour on frame
				cv::drawContours(contours_image, contours, i, color, 2, 8, hierarcy);
				/// Draw contour mass center
				cv::circle(contours_image, mc[i], 4, color, -1, 8, 0);
				/// Put label that this contour is pentagon and count
				setLabel(contours_image, "PENT", contours[i]);
			}
		}
		/// Put hexagon label
		else if (approx.size() == 6)
		{
			/// Go through all figures and verify its unique
			for (size_t j = 0; j < count_6.size(); j++)
			{
				/// If difference is small figure is not new
				if ((abs(mc[i].x - count_6[j].x) < min_difference) && (abs(mc[i].y - count_6[j].y) < min_difference))
				{
					/// Set flag to false to not draw and count it
					new_figure = false;
				}
			}
			/// If figure is new draw it and store its mass center
			if (new_figure)
			{
				/// Add new value
				count_6.push_back(mc[i]);

				/// Draw contour on frame
				cv::drawContours(contours_image, contours, i, color, 2, 8, hierarcy);
				/// Draw contour mass center
				cv::circle(contours_image, mc[i], 4, color, -1, 8, 0);
				/// Put label that this contour is hexagon and count
				setLabel(contours_image, "HEX", contours[i]);
			}
		}
		/// Else if it is circle
		else
		{
			/// Get contour area
			double area = cv::contourArea(contours[i]);
			/// Get contour bounding rectangle
			cv::Rect r = cv::boundingRect(contours[i]);
			/// Get circle radius
			int radius = r.width / 2;

			/// Check that circle is symmetric
			if ((abs(1 - (double)r.width / r.height) < 0.2) && (abs(1 - (area / (CV_PI * radius * radius))) < 0.2))
			{
				/// Go through all figures and verify its unique
				for (size_t j = 0; j < count_0.size(); j++)
				{
					/// If difference is small figure is not new
					if ((abs(mc[i].x - count_0[j].x) < min_difference) && (abs(mc[i].y - count_0[j].y) < min_difference))
					{
						/// Set flag to false to not draw and count it
						new_figure = false;
					}
				}
				/// If figure is new draw it and store its mass center
				if (new_figure)
				{
					/// Add new value
					count_0.push_back(mc[i]);

					/// Draw contour on frame
					cv::drawContours(contours_image, contours, i, color, 2, 8, hierarcy);
					/// Draw contour mass center
					cv::circle(contours_image, mc[i], 4, color, -1, 8, 0);
					/// Put label that this contour is circle and count
					setLabel(contours_image, "CIRCLE", contours[i]);
				}
			}
		}
	}
	if (CAM_OR_IMAGE)
	{
		/// Show each figure type quantity
		std::cout << "Triangles: " << count_3.size() << "/6" << std::endl
			<< "Rectangles: " << count_4.size() << "/5" << std::endl
			<< "Pentagons: " << count_5.size() << "/5" << std::endl
			<< "Hexagons: " << count_6.size() << "/3" << std::endl
			<< "Circles: " << count_0.size() << "/1" << std::endl;
	}
}

/// Put label - black text on white rectangle - in contour center
void Lib::setLabel(cv::Mat im, const std::string label, std::vector<cv::Point> contour)
{
	/// Set text font
	int font = CV_FONT_HERSHEY_SIMPLEX;
	/// Set text size
	double scale = 0.4;
	/// Set text thick
	int thick = 1;
	/// Set text baseline
	int baseline = 0;

	/// Create text properties object
	cv::Size text = cv::getTextSize(label, font, scale, thick, &baseline);
	/// Get contour bounding rectangle
	cv::Rect r = cv::boundingRect(contour);
	/// Set up a point inside rectangle
	cv::Point pt(r.x + ((r.width - text.width) / 2), r.y + ((r.height + text.height) / 2));
	/// Draw small white rectangle inside contour
	cv::rectangle(im, pt + cv::Point(0, baseline), pt + cv::Point(text.width, -text.height), CV_RGB(255, 255, 255), CV_FILLED);
	/// Put black label inside white rectangle
	cv::putText(im, label, pt, font, scale, CV_RGB(0, 0, 0), thick, 8);
}

/// Get red colour from HSV
void Lib::getRedColour()
{
	/// Change color space from RGB to HSV
	cv::cvtColor(input_image, hsv_image, CV_BGR2HSV);

	/// Threshold to get only red color
	cv::inRange(hsv_image, cv::Scalar(H, S, V), cv::Scalar(Hmax, Smax, Vmax), red_color_image);

	/// Form kernel for dilate and erode
	cv::Mat element = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5, 5));

	/// Prepare image for finding contours
	cv::dilate(red_color_image, red_color_image, element, cv::Point(-1, -1), 4);
	cv::erode(red_color_image, red_color_image, element, cv::Point(-1, -1), 2);
}

/// Show and update all parameters
void Lib::updateParameters()
{
	/// For HSV
	cv::createTrackbar("H", "Parameters", &H, 255);
	cv::createTrackbar("S", "Parameters", &S, 255);
	cv::createTrackbar("V", "Parameters", &V, 255);
	cv::createTrackbar("Hmax", "Parameters", &Hmax, 255);
	cv::createTrackbar("Smax", "Parameters", &Smax, 255);
	cv::createTrackbar("Vmax", "Parameters", &Vmax, 255);

	/// Create track bar for change Canny threshold value
	cv::createTrackbar("Canny", "Parameters", &thresh, max_thresh);

	/// Show all trackbars
	cv::imshow("Parameters", parameters);
}

/// Show named image
void Lib::showImage(std::string name)
{
	if (name == "input_image")
	{
		cv::imshow("Frame", input_image);
	}
	else if (name == "grayscale_image")
	{
		cv::imshow("Grayscale", grayscale_image);
	}
	else if (name == "blurred_grayscale_image")
	{
		cv::imshow("Blurred Grayscale", blurred_grayscale_image);
	}
	else if (name == "canny_image")
	{
		cv::imshow("Canny", canny_image);
	}
	else if (name == "circles_image")
	{
		cv::imshow("Circles", circles_image);
	}
	else if (name == "hsv_image")
	{
		cv::imshow("HSV", hsv_image);
	}
	else if (name == "red_color_image")
	{
		cv::imshow("Red color", red_color_image);
	}
	else if (name == "contours_image")
	{
		/// Show found contours
		cv::imshow("Contours", contours_image);
	}
}

/// Detect all circles on current frame, overlay all
/// and return modified image
void Lib::detectCircles()
{
	/// Array for storing recognised circles
	std::vector<cv::Vec3f> circles;
	/// Use Hough gradient to find circles in image
	cv::HoughCircles(blurred_grayscale_image, circles, cv::HOUGH_GRADIENT, 1, blurred_grayscale_image.rows / 8, 50, 50, 20, 100);

	/// Copy base image
	input_image.copyTo(circles_image);

	/// Draw all circles on color image
	for (size_t i = 0; i < circles.size(); i++)
	{
		/// Get circle center
		cv::Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
		/// Get circle radius
		int radius = cvRound(circles[i][2]);
		/// Draw circle center
		cv::circle(circles_image, center, 3, cv::Scalar(0, 255, 0), -1, 8, 0);
		/// Draw circle edge
		cv::circle(circles_image, center, radius, cv::Scalar(0, 0, 255), 3, 8, 0);
	}
}

/// Get grayscale, blurred and canny images from base on
void Lib::prepareImage()
{
	/// Resize
	cv::resize(input_image, input_image, cv::Size(640, 480));

	/// Modify color image to grayscale
	cv::cvtColor(input_image, grayscale_image, cv::COLOR_BGR2GRAY);

	/// Use Gaussian blur
	cv::GaussianBlur(grayscale_image, blurred_grayscale_image, cv::Size(7, 7), 1.5, 1.5);

	/// Canny edge detector
	cv::Canny(grayscale_image, canny_image, thresh, thresh * 2, 3);
}

/// Default destructor
Lib::~Lib()
{
}

/// Return flag to obtain image from camera or saved one
bool Lib::getCamOrImage()
{
	return CAM_OR_IMAGE;
}