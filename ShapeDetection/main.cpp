#include "lib.h"

int main()
{
	/// Define one class object for store all data
	Lib base;

	/// Loop for obtain new results based on
	/// parameters change
	while (true)
	{
		/// Read image or catch video frame
		base.readImage("stop_1.jpg");

		/// Get grayscale, blurred and canny image 
		/// from input one
		base.prepareImage();

		/// Detect all circles
		//base.detectCircles();

		/// Show and update HSV and Canny parameters
		base.updateParameters();

		/// Get all figures on image and count them
		//base.getFigures();

		/// Get red color from HSV image
		base.getRedColour();
		base.getROI();
		/// Show processed images
		base.showImage("input_image");
		//base.showImage("canny_image");
		//base.showImage("circles_image");
		//base.showImage("contours_image");
		base.showImage("red_color_image");

		//base.showImage("grayscale_image");
		//base.showImage("blurred_grayscale_image");
		//base.showImage("hsv_image");
		//base.showImage("grayscale_image");


		/// Delay for finish image processing for output
		if (!base.getCamOrImage())
		{
			/// Wait 60 ms
			if (cv::waitKey(60) >= 0)
			{
				/// Go to next cycle
				continue;
			}
		}
		else
		{
			/// Infinite wait
			cv::waitKey(0);
		}
	}
}